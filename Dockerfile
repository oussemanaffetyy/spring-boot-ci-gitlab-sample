From openjdk:11
VOLUME /tmp
ADD /target/*.jar app.jar
ENTRYPOINT ["java","-Djava.security.edg=file:/dev ./urandom","-jar","/app.jar"]
